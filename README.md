# jmmc-vollt-kubernetes

Deploy TAP services on k8s clusters

* Prepare blue and green ConfigMaps on top of [https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/JMMC/jmmc-vollt-server/-/tree/master/vollt](volt directory) 
 * kubectl create configmap tap-vollt-cfg-COLOR --from-file=vollt/META-INF/ --from-file=vollt/WEB-INF/
* Edit and configure your TAP instance
 * kubectl edit configmap tap-vollt-cfg-COLOR
* Deploy ingress and pod
 * kubectl apply -f blue-green-ingress.yml
 * kustomize build overlays/COLOR | kubectl apply -f -

